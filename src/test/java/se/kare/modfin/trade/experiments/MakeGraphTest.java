package se.kare.modfin.trade.experiments;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;

public class MakeGraphTest {

	@Test
	public void show() throws JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		ClassLoader classLoader = MakeGraphTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("t1.json");
		assertTrue(is != null);
		MakeGraph mg = new MakeGraph();
		mg.show(is);
		Thread.sleep(3000);
		//Thread.currentThread().join();
	}

}
