package se.kare.modfin.trade;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;

import se.kare.modfin.trade.experiments.MakeGraphTest;

public class OptimalTradeAlgoDemoTest {

	@Test
	public void test() throws JsonParseException, JsonMappingException, IOException {
		ClassLoader classLoader = MakeGraphTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("t1.json");
		assertTrue(is != null);
		OptimalTradeAlgoDemo.demo(is);
	}

}
