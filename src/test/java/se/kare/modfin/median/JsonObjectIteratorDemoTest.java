package se.kare.modfin.median;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class JsonObjectIteratorDemoTest {

	public final static int dist_start = 200;
	public final static int dist_width = 300;

	public static InputStream getStream() throws IOException {
		ClassLoader classLoader = JsonObjectIteratorDemoTest.class.getClassLoader();
		String contents = IOUtils.toString(classLoader.getResourceAsStream("s3.json"), "UTF-8");
		PipedOutputStream pos = new PipedOutputStream();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// This imitates some remote process and in particular how it is experienced locally
				// with sporadic arrival of bytes and various delays between them.
				byte[] b = contents.getBytes();
				int position = 0;
				try {
					while(position < b.length) {
						int dist_this = (int) ((Math.random()*((double) dist_width))+dist_start);
						System.out.format("This round bytes out %d%n", dist_this);
						int len = Math.min(dist_this, b.length - position);
						pos.write(b, position, len);
						position += len;
						Thread.sleep(len);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					pos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		return new PipedInputStream(pos);
	}

	@Test
	public void test() throws IOException {
		assertTrue(8.519035880580663 == JsonObjectIteratorDemo.demonstrateFunctionality(getStream()));
	}
}
