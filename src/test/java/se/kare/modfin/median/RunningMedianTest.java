package se.kare.modfin.median;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import se.kare.modfin.median.RunningMedian;

public class RunningMedianTest {

	// These tests are the same as on the download page
	// (Repeats from the RunningMedian class comments) https://gist.github.com/Vedrana/3675434
	@Test
	public void runningMedian() {
		RunningMedian streamMedian = new RunningMedian();
		
		streamMedian.addNumberToStream(1.0);
		assertTrue(1.0 == streamMedian.getMedian()); // should be 1.0
		
		streamMedian.addNumberToStream(5.0);
		streamMedian.addNumberToStream(10.0);
		streamMedian.addNumberToStream(12.0);
		streamMedian.addNumberToStream(2.0);
		assertTrue(5.0 == streamMedian.getMedian()); // should be 5.0
		
		streamMedian.addNumberToStream(3.0);
		streamMedian.addNumberToStream(8.0);
		streamMedian.addNumberToStream(9.0);
		assertTrue(6.5 == streamMedian.getMedian()); // should be 6.5
	}
	
}
