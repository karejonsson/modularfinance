package se.kare.modfin.median;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.Map;

import org.junit.Test;

public class JsonObjectIteratorTest {

	@Test
	public void test_s1() {
		ClassLoader classLoader = JsonObjectIteratorTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("s1.json");
		JsonObjectIterator joi = new JsonObjectIterator(is);
		int ctr = 0;
		while(joi.hasNext()) {
			ctr++;
			Map<String, Object> obj = joi.next();
			System.out.println("OBJ "+obj);
		}
		assertTrue(2 == ctr);
	}

	@Test
	public void test_s3() {
		ClassLoader classLoader = JsonObjectIteratorTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("s3.json");
		JsonObjectIterator joi = new JsonObjectIterator(is);
		int ctr = 0;
		while(joi.hasNext()) {
			Map<String, Object> obj = joi.next();
			System.out.println("OBJ "+obj);
			ctr++;
		}
		assertTrue(257 == ctr);
	}

}
