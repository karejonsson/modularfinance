package se.kare.modfin.median.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import se.kare.modfin.median.RunningMedian;
import se.kare.modfin.median.experiments.DateRateEntry;

public class GsonImplementationTest {

	@Test
	public void develop() throws IOException {
		ClassLoader classLoader = GsonImplementationTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("s1.json");
		String s1 = IOUtils.toString(is, StandardCharsets.UTF_8);
		//System.out.println(s1);

		Gson gson = new Gson();		 

		@SuppressWarnings("unchecked")
		ArrayList<DateRateEntry> entries = (ArrayList<DateRateEntry>) gson.fromJson(s1,
				new TypeToken<ArrayList<DateRateEntry>>() {
		}.getType());

		RunningMedian streamMedian = new RunningMedian();

		for(DateRateEntry dre : entries) {
			streamMedian.addNumberToStream(dre.getRates().getSEK()/dre.getRates().getUSD());
		}
		System.out.format("GSON: Running median %.3f%n", streamMedian.getMedian()); // should be 6.5

	}
	
}
