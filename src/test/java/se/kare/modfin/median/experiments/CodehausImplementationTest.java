package se.kare.modfin.median.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import se.kare.modfin.median.RunningMedian;

public class CodehausImplementationTest {

	@Test
	public void develop() throws IOException {
		ClassLoader classLoader = CodehausImplementationTest.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("s1.json");
		//String s1 = IOUtils.toString(is, StandardCharsets.UTF_8);
		//System.out.println(s1);

		ObjectMapper mapper = new ObjectMapper();
		List jsonList = mapper.readValue(is, List.class);

		RunningMedian streamMedian = new RunningMedian();

		for(Object dre : jsonList) {
			Map rates = (Map) ((Map) dre).get("rates");
			streamMedian.addNumberToStream(((Double) rates.get("SEK"))/((Double) rates.get("USD")));
			System.out.println(dre.toString());
		}
		System.out.format("Codehaus: Running median %.3f%n", streamMedian.getMedian()); // should be 6.5

	}

}
