package se.kare.modfin.median.experiments

import org.junit.Test

import se.kare.modfin.median.experiments.Main

class MainTest {

	@Test
	void test() {
		InputStream is = ClassLoader.class.getSystemResourceAsStream("s3.json")
		println "Main: Running median "+Main.getMedian(is)
	}

}
