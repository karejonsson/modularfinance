package se.kare.modfin.median;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

// Given a stream of unsorted integers, find the median element in sorted order at any given time.
// http://www.ardendertat.com/2011/11/03/programming-interview-questions-13-median-of-integer-stream/
// My download https://gist.github.com/Vedrana/3675434
public class RunningMedian {

	public Queue<Double> minHeap;
	public Queue<Double> maxHeap;
	public int numOfElements;
	
	public RunningMedian() {
		minHeap = new PriorityQueue<Double>();
		maxHeap = new PriorityQueue<Double>(10, new MaxHeapComparator()); 
		numOfElements = 0;
	}
	
	public void addNumberToStream(Double num) {
		synchronized(this) {
			maxHeap.add(num);
			if (numOfElements%2 == 0) {
				if (minHeap.isEmpty()) {
					numOfElements++;
					return;
				}
				else if (maxHeap.peek() > minHeap.peek()) {
					Double maxHeapRoot = maxHeap.poll();
					Double minHeapRoot = minHeap.poll();
					maxHeap.add(minHeapRoot);
					minHeap.add(maxHeapRoot);
				} 
			} else {
				minHeap.add(maxHeap.poll());
			}
			numOfElements++;
		}
	}
	
	public Double getMedian() {
		synchronized(this) {
			if (numOfElements%2 != 0)
				return new Double(maxHeap.peek());
			else
				return (maxHeap.peek() + minHeap.peek()) / 2.0; 
		}
	}
	
	private class MaxHeapComparator implements Comparator<Double> {
		@Override
		public int compare(Double o1, Double o2) {
			return o2 >= o1 ? 1 : -1;
		}
	}
	
	public static void main(String[] args) {
		RunningMedian streamMedian = new RunningMedian();
		
		streamMedian.addNumberToStream(1.0);
		System.out.format("1? %.1f%n", streamMedian.getMedian()); // should be 1
		
		streamMedian.addNumberToStream(5.0);
		streamMedian.addNumberToStream(10.0);
		streamMedian.addNumberToStream(12.0);
		streamMedian.addNumberToStream(2.0);
		System.out.format("5? %.1f%n", streamMedian.getMedian()); // should be 5
		
		streamMedian.addNumberToStream(3.0);
		streamMedian.addNumberToStream(8.0);
		streamMedian.addNumberToStream(9.0);
		System.out.format("6.5? %.1f%n", streamMedian.getMedian()); // should be 6.5
	}
}