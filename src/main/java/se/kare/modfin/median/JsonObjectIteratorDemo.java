package se.kare.modfin.median;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class JsonObjectIteratorDemo {
	
	public static Double demonstrateFunctionality(InputStream is) throws IOException {
		RunningMedian rm = new RunningMedian();
		JsonObjectIterator joi = new JsonObjectIterator(is);
		Runnable runnable = new Runnable() {
			// Some process locally that reads the stream and is proven able to handle it 
			// with only next and hasNext methods and no introduced types/classes.
			@Override
			public void run() {
				while(joi.hasNext()) {
					Map<String, Object> obj = joi.next();
					Double sek = (Double) ((Map<String, Object>) obj.get("rates")).get("SEK");
					rm.addNumberToStream(sek);
				}
				try {
					joi.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		Thread t = new Thread(runnable);
		t.start();
		try {
			// Some other local process that reads the running median while it is changing 
			// by the reads of the other local process. The reference is shared, here the reference rm.
			Thread.sleep(850);
			while(joi.hasNext()) {
				System.out.format("Running median %.3f%n", rm.getMedian());
				Thread.sleep(850);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return rm.getMedian();
	}
	
	public static void main(String args[]) throws InterruptedException, JsonParseException, JsonMappingException, IOException, ParseException {
		URL url = new URL("http://fx.modfin.se/2016-01-01/2016-12-31?symbols=sek&base=USD");
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		Double out = demonstrateFunctionality(conn.getInputStream());
		System.out.format("Final running median %.3f%n", out);
	}


}
