package se.kare.modfin.median.experiments;

public class RateEntry {

	/*
	  {
	    "base": "EUR",
	    "date": "2010-01-04",
	    "rates": {
	      "USD": 1.4389,
	      "SEK": 10.193
	    }
	  }
	  	 */

	private Double USD = null;
	public Double getUSD() {
		return USD;
	}
	public void setUSD(Double uSD) {
		USD = uSD;
	}
	public Double getSEK() {
		return SEK;
	}
	public void setSEK(Double sEK) {
		SEK = sEK;
	}
	private Double SEK = null;
}
