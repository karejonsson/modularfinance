package se.kare.modfin.median.experiments;

import java.util.Date;

public class DateRateEntry {

	/*
  {
    "base": "EUR",
    "date": "2010-01-04",
    "rates": {
      "USD": 1.4389,
      "SEK": 10.193
    }
  }
  	 */
	
	private String base = null;
	private Date date = null;
	private RateEntry rates = null;
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public RateEntry getRates() {
		return rates;
	}
	public void setRateentry(RateEntry rates) {
		this.rates = rates;
	}


}
