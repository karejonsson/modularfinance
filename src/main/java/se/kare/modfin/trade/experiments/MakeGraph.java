package se.kare.modfin.trade.experiments;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class MakeGraph {

	private TimeSeries maxserie = new TimeSeries("Max", Day.class);
	private TimeSeries minserie = new TimeSeries("Min", Day.class);
	private TimeSeriesCollection tradedata = new TimeSeriesCollection();
	private JFreeChart leanchart = null;
	
	public MakeGraph() {
		tradedata.addSeries(minserie);
		tradedata.addSeries(maxserie);
    	
        leanchart = ChartFactory.createTimeSeriesChart(
                "OMXS30", 
                "", // Text below chart
                "price", 
                tradedata,
                true,
                true,
                false
        );
        final XYPlot plot = leanchart.getXYPlot();
        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
        domainaxis.setAutoRange(true);
        domainaxis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
        valueaxis.setAutoRangeIncludesZero(false);
	}
	
	public void addMinValue(Date day, Double price) {
		minserie.addOrUpdate(new Day(day), price);
	}
	
	public void addMaxValue(Date day, Double price) {
		maxserie.addOrUpdate(new Day(day), price);
	}
	
	public void show() {
		ChartPanel tiplabel = new ChartPanel(leanchart);
		JFrame f = new JFrame();
	    f.setSize(800, 500);
	    f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    f.setVisible(true);
	    f.getContentPane().add(tiplabel);
	    f.setVisible(true);
	}

	public void show(InputStream is) throws JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		ObjectMapper mapper = new ObjectMapper();
		Map jsonMap = mapper.readValue(is, Map.class);
		
		System.out.println("Info "+jsonMap.get("info"));

		List<Map> data = (List<Map>) jsonMap.get("data");
		System.out.println("Entries "+data.size());
		
		MakeGraph gt = new MakeGraph();
		
		for(Map m : data) {
			String strDate = ""+m.get("quote_date");
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	        Date dateStr = formatter.parse(strDate);

	        Object o = m.get("low");
			Double low = o instanceof Integer ? new Double((Integer) o) : (Double) o;
			o = m.get("high");
			Double high = o instanceof Integer ? new Double((Integer) o) : (Double) o;
			gt.addMinValue(dateStr, low);
			gt.addMaxValue(dateStr, high);
		}
		
		gt.show();
	}
	
	public static void main(String args[]) throws InterruptedException, JsonParseException, JsonMappingException, IOException, ParseException {
		URL url = new URL("https://www.modularfinance.se/api/puzzles/index-trader.json");
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		MakeGraph mg = new MakeGraph();
		mg.show(conn.getInputStream());
		Thread.currentThread().join();
	}
	
}
