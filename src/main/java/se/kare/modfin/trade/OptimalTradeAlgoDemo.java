package se.kare.modfin.trade;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import se.kare.modfin.trade.experiments.MakeGraph;

public class OptimalTradeAlgoDemo {

	public static void demo(InputStream is) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		Map jsonMap = mapper.readValue(is, Map.class);
		
		List<Map> data = (List<Map>) jsonMap.get("data");
		
		int firstPos = data.size()-1;
		Map m = data.get(firstPos);

		Object o = m.get("low");
		double firstMinValue = o instanceof Integer ? new Double((Integer) o) : (Double) o;
		o = m.get("high");
		double firstMaxValue = o instanceof Integer ? new Double((Integer) o) : (Double) o;

		OptimalTradeAlgo ota = new OptimalTradeAlgo(data.size()-1, firstMinValue, firstMaxValue, ""+m.get("quote_date"));
		
		for(int pos = firstPos-1 ; pos >= 0 ; pos--) {
			m = data.get(pos);
			o = m.get("low");
			double currentMinValue = o instanceof Integer ? new Double((Integer) o) : (Double) o;
			o = m.get("high");
			double currentMaxValue = o instanceof Integer ? new Double((Integer) o) : (Double) o;
			ota.add(pos, currentMinValue, currentMaxValue, ""+m.get("quote_date"));
		}

		ota.printOutcome();
	}
	
	public static void main(String args[]) throws InterruptedException, JsonParseException, JsonMappingException, IOException, ParseException {
		URL url = new URL("https://www.modularfinance.se/api/puzzles/index-trader.json");
		URLConnection conn = url.openConnection();
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		demo(conn.getInputStream());
	}

}
