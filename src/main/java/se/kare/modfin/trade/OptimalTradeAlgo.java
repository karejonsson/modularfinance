package se.kare.modfin.trade;

public class OptimalTradeAlgo {

	private int bestMinPos;
	private int bestMaxPos;
	private double bestMinValue;
	private double bestMaxValue;
	private double bestGain;
	private String bestBuyDate;
	private String bestSellDate;
	
	private int candidateMinPos;
	private int candidateMaxPos;
	private double candidateMinValue;
	private double candidateMaxValue;
	private double candidateGain;
	private String candidateBuyDate;
	private String candidateSellDate;

	public OptimalTradeAlgo(int firstPos, double firstMinValue, double firstMaxValue, String first_quote_date) {
		bestMinPos = bestMaxPos = firstPos;
		bestMinValue = firstMinValue;
		bestMaxValue = firstMaxValue;
		bestGain = firstMaxValue - firstMinValue;
		bestBuyDate = bestSellDate = first_quote_date;
		
		candidateMinPos = bestMinPos;
		candidateMaxPos = bestMaxPos;
		candidateMinValue = bestMinValue;
		candidateMaxValue = bestMaxValue;
		candidateGain = bestGain;
		candidateBuyDate = bestBuyDate;
		candidateSellDate = bestSellDate;
	}
	
	/**
	 * 
	 * @param pos Position in the vector. To remember this is a service from the algorithm.
	 * @param minValue Minimal value for the day
	 * @param maxValue Maximal value for the day
	 * @param quote_date Date as string specifying the day
	 */
	public void add(int pos, double minValue, double maxValue, String quote_date) {
		if(maxValue - candidateMinValue > candidateGain) {
			// A better selling point is found. Moves candidates sell info to this.
			candidateMaxPos = pos;
			candidateMaxValue = maxValue;
			candidateGain = candidateMaxValue - candidateMinValue;
			candidateSellDate = quote_date;
			System.out.format("Move candidate sell to %d, date %s, gain %.3f%n", pos, quote_date, candidateGain);
		}
		if(minValue < candidateMinValue) {
			// A better buying point is found. Moves candidates buy info to this.
			// Since this has remained a candidate it can be forgotten since the so far best is better 
			// anyway. The search continues with this new and lower bottom as buy point.
			candidateMinPos = pos;
			candidateMinValue = minValue;
			candidateMaxPos = pos;
			candidateMaxValue = maxValue;
			candidateGain = maxValue - minValue;
			candidateBuyDate = quote_date;
			candidateSellDate = quote_date;
			System.out.format("Move candidate buy to %d, date %s, gain %.3f%n", pos, quote_date, candidateGain);
			return;
		}
		if(candidateGain > bestGain) {
			// The candidate is better than the previous best. Updates.
			bestMinPos = candidateMinPos;
			bestMaxPos = candidateMaxPos;
			bestMinValue = candidateMinValue;
			bestMaxValue = candidateMaxValue;
			bestGain = candidateGain;
			bestBuyDate = candidateBuyDate;
			bestSellDate = candidateSellDate;
			System.out.format("Move best: min pos = %d, max pos %d, gain %.3f%n", bestMinPos, bestMaxPos, bestGain);
		}
	}
	
	public void printOutcome() {
		System.out.format("Final Move best: min pos = %d, max pos %d, gain %.3f%n", bestMinPos, bestMaxPos, bestGain);
        System.out.format("Buy %s for %.3f, sell %s for %.3f%n",bestBuyDate, bestMinValue, bestSellDate, bestMaxValue);
	}

}
