package se.kare.modfin.median.experiments

import groovy.json.JsonSlurper
import se.kare.modfin.median.experiments.Main

import java.util.PriorityQueue
import java.util.Queue

class Main {
	
	Queue<Double> minHeap = new PriorityQueue<Double>()
	Queue<Double> maxHeap = new PriorityQueue<Double>(10, {d1, d2 -> d2 >= d1 ? 1 : -1})
	boolean even = true
	
	Double getMedian() {
		!even ? maxHeap.peek() : (maxHeap.peek() + minHeap.peek()) / 2.0
	}

	void add(Double num) {
		maxHeap.add(num)
		if (even) {
			if (minHeap.isEmpty()) {
				even = !even
				return;
			}
			else if (maxHeap.peek() > minHeap.peek()) {
				Double maxHeapRoot = maxHeap.poll()
				maxHeap.add(minHeap.poll())
				minHeap.add(maxHeapRoot)
			}
		} else {
			minHeap.add(maxHeap.poll())
		}
		even = !even
	}
	
	static Double getMedian(InputStream is) {
		def main = new Main()
		new JsonSlurper().parse(is).each{it -> main.add((Double)it.rates.SEK)}
		main.getMedian()
	}
	
	static void main(String[] args) {
		InputStream is = new URL("http://fx.modfin.se/2016-01-01/2016-12-31?symbols=sek&base=USD").openStream();
		println "Main: Running median "+getMedian(is)
	}
	
}
